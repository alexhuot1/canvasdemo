class Anchor {
   constructor(x,y){
      this.x = x;
      this.y = y;
   }

   draw(ctx) {
      ctx.beginPath();
      ctx.moveTo(this.x, this.y)
      ctx.lineTo(this.x, this.y + 20)
      ctx.stroke();
   }
}

class Selector extends React.Component {
   constructor(props) {
      super(props);
   }

   render() {
      const divStyle = {
        color: 'white',
        backgroundColor: 'black',
        height: "50px",
        width: "100px",
        position: 'absolute',
        top: this.props.top || 0,
        left: this.props.left || 0,
        display: "flex"
      };
      return (
         <div style={divStyle}>
            <button onClick={this.props.incrementCount} style={{flex: "1 1 auto"}}>+</button>
            <button onClick={this.props.decrementCount} style={{flex: "1 1 auto"}}>-</button>
         </div>
      );
   }
}

class DrawingZone extends React.Component {

   constructor(props){
      super(props);
      this.state = {
         anchorPoints: [
            new Anchor(100,100),
            new Anchor(200,100),
            new Anchor(250,100),
            new Anchor(300,100),
            new Anchor(500,100)
         ],
         positionClicked: {
            x: 0,
            y: 0
         }
      }

      // Sort du state
      this.circle = {x: 50, y: 110, radius: 10}

      this.canvas = React.createRef();
      this.canvasContext = null;
      this.CANVAS_TOP = 0;
      this.CANVAS_LEFT = 0;
      this.$2pi = 2 * Math.PI;
      this.isCircleBeingDragged = false
      this.draw = this.draw.bind(this);
      this.onMouseDown = this.onMouseDown.bind(this);
      this.onMouseMove = this.onMouseMove.bind(this);
      this.onMouseUp = this.onMouseUp.bind(this);
      this.onClick = this.onClick.bind(this);
   }

   onMouseDown(e) {
      var x = e.clientX - this.CANVAS_LEFT;
      var y = e.clientY - this.CANVAS_TOP;
      this.isCircleBeingDragged = this.isInsideCircle(x, y, this.circle);
      if (!this.isCircleBeingDragged){
         this.setState({positionClicked: {x: e.clientX, y:e.clientY}})
      }
   };

   onMouseMove(e) {
      if (this.isCircleBeingDragged) {
         let x = e.clientX - this.CANVAS_LEFT;
         let y = e.clientY - this.CANVAS_TOP;
         this.circle.x = x;

         for (let i = 0; i < this.state.anchorPoints.length; ++i) {
           if (this.shouldSnap(this.circle, this.state.anchorPoints[i])){
             this.circle.x = this.state.anchorPoints[i].x;
             break;
           }
         }
      }
   }

   onMouseUp(e) {
      if (this.isCircleBeingDragged)
         this.circle.x = this.findClosestAnchor().x
      this.isCircleBeingDragged = false;
   }

   onClick(e) {

   }

   findClosestAnchor() {
      var closestAnchor = this.state.anchorPoints[0];

      for (var i = 1; i < this.state.anchorPoints.length; ++i) {
         if (Math.abs(this.circle.x - this.state.anchorPoints[i].x) < Math.abs(this.circle.x - closestAnchor.x)) {
            closestAnchor = this.state.anchorPoints[i]
         }
      }

      return closestAnchor;
   }

   shouldSnap(circle, anchor){
      return (Math.abs(circle.x - anchor.x) < circle.radius);
   }

   draw() {
      this.canvasContext.clearRect(0, 0, 800, 800);

      this.canvasContext.lineWidth = 1;
      this.state.anchorPoints.forEach(anchor => anchor.draw(this.canvasContext))

      // Add circle
      this.canvasContext.beginPath();
      this.canvasContext.fillStyle = "#ff2b2b";
      this.canvasContext.arc(this.circle.x, this.circle.y, this.circle.radius, 0, this.$2pi)
      this.canvasContext.lineWidth = 4;
      this.canvasContext.stroke();
      this.canvasContext.fill();

      requestAnimationFrame(this.draw);
   }

   isInsideCircle(x, y, circle) {
      var dx = x - circle.x;
      var dy = y - circle.y;
      var distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
      return (distance <= circle.radius);
   };

   shouldSnap(circle, anchor){
      return (Math.abs(circle.x - anchor.x) < circle.radius);
   }

   componentDidMount() {
      this.CANVAS_TOP = this.canvas.current.offsetTop;
      this.CANVAS_LEFT = this.canvas.current.offsetLeft;
      this.canvasContext = this.canvas.current.getContext("2d")
      requestAnimationFrame(this.draw);
   }

   render() {
      return (
         <div>
            <Selector top={this.state.positionClicked.y} left={this.state.positionClicked.x} incrementCount={this.props.incrementCount} decrementCount={this.props.decrementCount}/>
            <canvas onClick={this.onClick} onMouseDown={this.onMouseDown} onMouseMove={this.onMouseMove} onMouseUp={this.onMouseUp} ref={this.canvas}id="canvas" width="800" height="800"></canvas>
         </div>
      );
   }
}

class Test extends React.Component {

   constructor(props) {
      super(props);
      this.state = {
         count: 0
      }

      this.incrementCount = this.incrementCount.bind(this)
      this.decrementCount = this.decrementCount.bind(this)
   }

   incrementCount() {
      this.setState((preState) => (
         {count: preState.count + 1}
      ))
   }

   decrementCount() {
      this.setState((preState) => (
         {count: preState.count - 1}
      ))
   }

   render() {
      return (
         <div>
            <span>{this.state.count}</span>
            <DrawingZone incrementCount={this.incrementCount} decrementCount={this.decrementCount}/>
         </div>
      );
   }

}

ReactDOM.render(<Test/>, document.getElementById('root'));
